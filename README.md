# casnode-web-new

This repo is a pure frontend project based on React + Ant Design. It  uses https://github.com/casbin/casnode (https://gitlab.summer-ospp.ac.cn/summer2021/210210899-2/-/tree/master) code as the Go backend. (on branch master)

This branch contains mid-term result.
To see final result, please checkout to master branch .